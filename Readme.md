# 🐳 Docker + PHP 8.2 + MySQL + Nginx + Symfony 6.2

## Description

This is a complete stack for running Symfony 6.2 into Docker containers using docker-compose tool.

It is composed by 3 containers:

- `nginx`, acting as the webserver.
- `php`, the PHP-FPM container with the 8.2 version of PHP.
- `db` which is the MySQL database container with a **MySQL 8.0** image.

## Installation

1. 😀 Clone this repo.

2. If you are working with Docker Desktop for Mac, ensure **you have enabled `VirtioFS` for your sharing implementation**. `VirtioFS` brings improved I/O performance for operations on bind mounts. Enabling VirtioFS will automatically enable Virtualization framework.

3. Create the file `./.docker/.env.nginx.local` using `./.docker/.env.nginx` as template. The value of the variable `NGINX_BACKEND_DOMAIN` is the `server_name` used in NGINX.

4. Run `docker compose up -d` to start containers.

5. You should work inside the `php` container. This project is configured to work with [Remote Container](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension for Visual Studio Code, so you could run `Reopen in container` command after open the project.

6. Inside the `php` container, run `composer install` to install dependencies from `/var/www/symfony` folder.

7. Use the following value for the DATABASE_URL environment variable:

    ```
    DATABASE_URL=mysql://root:SuperSecretRootPw123@cc-db-1:3306/code_challenge?serverVersion=8.0.33&charset=utf8mb4
    ```

8. Run the following command in the cc-php-1 docker container to create the database `php bin/console d:s:u --force --dump-sql`
 
9. Run the following command in the cc-php-1 docker container to populate the database `php bin/console hautelook:fixtures:load`

10. Download Postman and access the following endpoints with the following Test user `api-token: kDgjFpIxn7ReP2BENBjyOBLEB2DURNoU87FnV5HYG3KoDj1nbTCm`.
   1. Vehicle endpoints:
      1. `GET /api/vehicle` - Return a collection of vehicles
      2. `PATCH /api/vehicle/{id}` - Update a vehicle
      3. `DELETE /api/vehicle/{id}` - Delete vehicle
      4. `GET /api/vehicle/{id}` - Get vehicle detail
   2. Manufacturer endpoints:
      1. `GET /manufacturer` - Return a list of manufacturers
         Query Params:
         1. `name` - Manufacturer name
         2. `vehicleName` - Vehicle name
         3. `vehicleType` - Vehicle type (car|motorcycle|truck|bus)
         4. `page` - Current page
         5. `limit` - Results per page
         6. `offset` - Current offset, overwrite page value
      2. `GET /manufacturer/{id}` - Return manufacturer detail

You could change the name, user and password of the database in the `env` file at the root of the project.

