<?php

namespace App\Pagination;

use JMS\Serializer\Annotation as Serializer;

class PaginatedCollection
{
    #[Serializer\Groups(["list","detail"])]
    private int $total;

    #[Serializer\Groups(["list","detail"])]
    private int $count;

    #[Serializer\Groups(["list","detail"])]
    private array $items;

    #[Serializer\Groups(["list","detail"])]
    private int $pages;
    #[Serializer\Groups(["list","detail"])]
    private array $links;
    private int $page;

    public function __construct(array $items, int $total, int $limit)
    {
        $this->total = $total;
        $this->count = count($items);
        $this->items = $items;
        $this->pages = (int) ceil($total / $limit);
    }

    public function addLink($ref, $url): self
    {
        $this->links[$ref] = $url;

        return $this;
    }

    public function getLinks(): array
    {
        return $this->links;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function setItems(array $items)
    {
        $this->items = $items;

        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getPages(): int
    {
        return $this->pages;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }
}
