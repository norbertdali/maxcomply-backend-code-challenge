<?php

namespace App\Form;

use App\Entity\Engine;
use App\Entity\Manufacturer;
use App\Entity\Vehicle;
use App\Repository\EngineRepository;
use App\Repository\ManufacturerRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class VehicleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('topSpeed',  NumberType::class, [
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('dimension', NumberType::class, [
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => Vehicle::TYPES,
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('engine', EntityType::class, [
                'class' => Engine::class,
                'query_builder' => static function (EngineRepository $repository) {
                    return $repository->createQueryBuilder('e')
                        // return only those params which are needed for decision
                        // let's say in my case this 3 is enough to decide
                        ->select('partial e.{id, engineCapacity, fuelType, horsePower}');
                },
                'constraints' => [
                    new NotBlank()
                ],
            ])
            ->add('manufacturer', EntityType::class, [
                'class' => Manufacturer::class,
                'query_builder' => static function (ManufacturerRepository $repository) {
                    return $repository->createQueryBuilder('m')
                        ->select('partial m.{id, name}');
                },
                'constraints' => [
                    new NotBlank()
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'method' => 'GET',
            'csrf_protection' => false,
            'data_class' => Vehicle::class,
        ]);
    }
}
