<?php

namespace App\Form;

use App\Model\ManufacturerSearch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ManufacturerSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add(
                'name',
                TextType::class,
            )
            ->add(
                'vehicleName',
                TextType::class
            )
            ->add(
                'vehicleType',
                TextType::class
            )
        ;

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'method' => 'GET',
            'csrf_protection' => false,
            'data_class' => ManufacturerSearch::class,
        ]);
    }
}
