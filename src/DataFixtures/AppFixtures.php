<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\ORM\AppNativeLoader;
use Nelmio\Alice\Throwable\LoadingThrowable;

class AppFixtures extends Fixture
{
    /**
     * Generate data fixtures for local development
     * @param ObjectManager $manager
     * @throws LoadingThrowable
     */
    public function load(ObjectManager $manager): void
    {
        $aliceFolder = __DIR__.'/../../fixtures';

        $loader = new AppNativeLoader;
        $objectSet = $loader->loadFiles([
            $aliceFolder.'/test.yml',
        ])->getObjects();

        foreach($objectSet as $object) {
            $manager->persist($object);
        }

        $manager->flush();
    }
}
