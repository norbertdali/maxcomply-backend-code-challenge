<?php

namespace App\DataFixtures\ORM;

namespace App\DataFixtures\ORM;

use Faker\Factory as FakerGeneratorFactory;
use Faker\Provider\Base as FakerDataProvider;
use Nelmio\Alice\Faker\Provider\AliceProvider;
use Nelmio\Alice\Loader\NativeLoader;
use Faker\Generator as FakerGenerator;

class AppNativeLoader extends NativeLoader
{
    public const LOCALE = 'en_EN';

    protected function createFakerGenerator(): FakerGenerator
    {
        // Localized Fake Data
        $generator = FakerGeneratorFactory::create(static::LOCALE);
        $generator->addProvider(new AliceProvider);
        $generator->seed($this->getSeed());

        // Alice: add methods
        $generator->addProvider(new FakerDataProvider($generator));

        return $generator;
    }
}
