<?php

namespace App\Entity;

use App\Repository\VehicleRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\Entity(repositoryClass: VehicleRepository::class)]
class Vehicle
{
    /**
     * Used in knp_dictionary.yml
     */
    public const TYPE_CAR = 'car';
    public const TYPE_MOTORCYCLE = 'motorcycle';
    public const TYPE_TRUCK = 'truck';
    public const TYPE_BUS = 'bus';
    public const TYPES = [
        self::TYPE_MOTORCYCLE,
        self::TYPE_CAR,
        self::TYPE_TRUCK,
        self::TYPE_BUS,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Serializer\Groups(["list", "detail"])]
    #[Assert\NotBlank()]
    #[Assert\NotNull()]
    private ?string $name = null;

    #[Serializer\Groups(["detail"])]
    #[ORM\Column(type: "integer", length: 11)]
    private ?int $topSpeed = null;
    #[ORM\Column(type: "integer", length: 11)]
    #[Serializer\Groups(["detail"])]
    #[Assert\NotBlank()]
    private ?int $dimension = null;

    #[ORM\Column(length: 50)]
    #[Serializer\Groups(["list", "detail"])]
    #[Assert\NotBlank()]
    private ?string $type = null;

    #[ORM\ManyToOne(inversedBy: 'vehicles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Serializer\Groups(["list", "detail"])]
    #[Assert\NotBlank()]
    private ?Engine $engine = null;

    #[ORM\ManyToOne(inversedBy: 'vehicles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Serializer\Groups(["list", "detail"])]
    #[Assert\NotBlank()]
    private ?Manufacturer $manufacturer = null;

    #[ORM\ManyToOne(inversedBy: 'vehicles')]
    #[ORM\JoinColumn(nullable: false)]
    #[Serializer\Groups(["list", "detail"])]
    #[Assert\NotBlank()]
    private ?VehicleModel $vehicleModel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getEngine(): ?Engine
    {
        return $this->engine;
    }

    public function setEngine(?Engine $engine): static
    {
        $this->engine = $engine;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): static
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTopSpeed(): ?int
    {
        return $this->topSpeed;
    }

    public function setTopSpeed(?int $topSpeed): self
    {
        $this->topSpeed = $topSpeed;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDimension(): ?int
    {
        return $this->dimension;
    }

    public function setDimension(?int $dimension): self
    {
        $this->dimension = $dimension;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return VehicleModel|null
     */
    public function getVehicleModel(): ?VehicleModel
    {
        return $this->vehicleModel;
    }

    /**
     * @param VehicleModel|null $vehicleModel
     * @return $this
     */
    public function setVehicleModel(?VehicleModel $vehicleModel): static
    {
        $this->vehicleModel = $vehicleModel;

        return $this;
    }
}
