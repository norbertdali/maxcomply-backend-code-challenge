<?php

namespace App\Entity;

use App\Repository\VehicleModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

#[ORM\Entity(repositoryClass: VehicleModelRepository::class)]
class VehicleModel
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Serializer\Groups(["list", "detail"])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'vehicleModels')]
    #[ORM\JoinColumn(nullable: false)]
    #[Serializer\Groups(["detail"])]
    private ?Manufacturer $manufacturer = null;

    #[ORM\OneToMany(mappedBy: 'vehicleModel', targetEntity: ProductionYear::class, orphanRemoval: true)]
    private Collection $productionYears;

    #[ORM\OneToMany(mappedBy: 'manufacturer', targetEntity: Vehicle::class, orphanRemoval: true)]
    private Collection $vehicles;

    public function __construct()
    {
        $this->productionYears = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): static
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * @return Collection<int, ProductionYear>
     */
    public function getProductionYears(): Collection
    {
        return $this->productionYears;
    }

    public function addProductionYear(ProductionYear $productionYear): static
    {
        if (!$this->productionYears->contains($productionYear)) {
            $this->productionYears->add($productionYear);
            $productionYear->setVehicleModel($this);
        }

        return $this;
    }

    public function removeProductionYear(ProductionYear $productionYear): static
    {
        if ($this->productionYears->removeElement($productionYear) && $productionYear->getVehicleModel() === $this) {
            $productionYear->setVehicleModel(null);
        }

        return $this;
    }


    /**
     * @return Collection<int, VehicleModel>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setManufacturer($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle) && $vehicle->getManufacturer() === $this) {
            $vehicle->setManufacturer(null);
        }

        return $this;
    }
}
