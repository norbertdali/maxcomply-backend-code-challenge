<?php

namespace App\Entity;

use App\Repository\ManufacturerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

#[ORM\Entity(repositoryClass: ManufacturerRepository::class)]
class Manufacturer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Serializer\Groups(["list", "detail"])]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Serializer\Groups(["detail"])]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'manufacturer', targetEntity: VehicleModel::class, orphanRemoval: true)]
    private Collection $vehicleModels;

    #[ORM\OneToMany(mappedBy: 'manufacturer', targetEntity: Vehicle::class, orphanRemoval: true)]
    private Collection $vehicles;

    public function __construct()
    {
        $this->vehicleModels = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, VehicleModel>
     */
    public function getVehicleModels(): Collection
    {
        return $this->vehicleModels;
    }

    public function addVehicleModel(VehicleModel $vehicleModel): static
    {
        if (!$this->vehicleModels->contains($vehicleModel)) {
            $this->vehicleModels->add($vehicleModel);
            $vehicleModel->setManufacturer($this);
        }

        return $this;
    }

    public function removeVehicleModel(VehicleModel $vehicleModel): static
    {
        if ($this->vehicleModels->removeElement($vehicleModel) && $vehicleModel->getManufacturer() === $this) {
            $vehicleModel->setManufacturer(null);
        }

        return $this;
    }


    /**
     * @return Collection<int, VehicleModel>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setManufacturer($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle) && $vehicle->getManufacturer() === $this) {
            $vehicle->setManufacturer(null);
        }

        return $this;
    }
}
