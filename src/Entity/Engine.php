<?php

namespace App\Entity;

use App\Repository\EngineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

#[ORM\Entity(repositoryClass: EngineRepository::class)]
class Engine
{
    /**
     * Used in knp_dictionary.yml
     */
    public const FUEL_TYPE_PETROL = 1;
    public const FUEL_TYPE_DIESEL = 2;
    // Compressed Natural Gas (CNG)
    public const FUEL_TYPE_CNG = 3;
    public const FUEL_TYPE_BIO_DISEL = 4;
    // Liquid Petroleum Gas (LPG)
    public const FUEL_TYPE_LPG = 5;
    public const FUEL_TYPE_ETHANOL = 6;

    // enums
    public const SPEED_TYPE_KM = 'km';
    public const SPEED_TYPE_MPH = 'mph';

    public const POSITION_FRONT = 'front';
    public const POSITION_MID = 'mid';
    public const POSITION_REAR = 'rear';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $id = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $engineCapacity = null;

    #[ORM\Column(type: Types::FLOAT)]
    #[Serializer\Groups(["list", "detail"])]
    private ?float $bore = null;

    #[ORM\Column(type: Types::FLOAT)]
    #[Serializer\Groups(["list", "detail"])]
    private ?float $stroke = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $fuelType = null;

    #[ORM\Column(nullable: true)]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $numberOfCylinders = null;

    #[ORM\Column(type: Types::STRING, length: 100)]
    #[Serializer\Groups(["list", "detail"])]
    private ?string $position = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $horsePower = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Serializer\Groups(["list", "detail"])]
    private ?int $numberOfSpeed = null;

    #[ORM\Column(type: Types::STRING, length: 3)]
    #[Serializer\Groups(["list", "detail"])]
    private ?string $speedType = null;

    #[ORM\OneToMany(mappedBy: 'engine', targetEntity: Vehicle::class)]
    private Collection $vehicles;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFuelType(): ?int
    {
        return $this->fuelType;
    }

    public function setFuelType(int $fuelType): static
    {
        $this->fuelType = $fuelType;

        return $this;
    }

    public function getNumberOfCylinders(): ?int
    {
        return $this->numberOfCylinders;
    }

    public function setNumberOfCylinders(?int $numberOfCylinders): static
    {
        $this->numberOfCylinders = $numberOfCylinders;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): static
    {
        $this->position = $position;

        return $this;
    }

    public function getEngineCapacity(): ?int
    {
        return $this->engineCapacity;
    }

    public function setEngineCapacity(int $engineCapacity): static
    {
        $this->engineCapacity = $engineCapacity;

        return $this;
    }

    public function getHorsePower(): ?int
    {
        return $this->horsePower;
    }

    public function setHorsePower(int $horsePower): static
    {
        $this->horsePower = $horsePower;

        return $this;
    }

    public function getNumberOfSpeed(): ?int
    {
        return $this->numberOfSpeed;
    }

    public function setNumberOfSpeed(int $numberOfSpeed): static
    {
        $this->numberOfSpeed = $numberOfSpeed;

        return $this;
    }

    /**
     * @return Collection<int, Vehicle>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setEngine($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle) && $vehicle->getEngine() === $this) {
            $vehicle->setEngine(null);
        }

        return $this;
    }

    /**
     * @return float|null
     */
    public function getBore(): ?float
    {
        return $this->bore;
    }

    /**
     * @param float|null $bore
     */
    public function setBore(?float $bore): void
    {
        $this->bore = $bore;
    }

    /**
     * @return float|null
     */
    public function getStroke(): ?float
    {
        return $this->stroke;
    }

    /**
     * @param float|null $stroke
     */
    public function setStroke(?float $stroke): void
    {
        $this->stroke = $stroke;
    }

    /**
     * @return string|null
     */
    public function getSpeedType(): ?string
    {
        return $this->speedType;
    }

    /**
     * @param string|null $speedType
     */
    public function setSpeedType(?string $speedType): void
    {
        $this->speedType = $speedType;
    }
}
