<?php

namespace App\Service;

use App\Pagination\PaginatedCollection;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class PaginationFactory
{
    private const LIMIT = 10;
    private const UNEXPECTED_LIMIT = 0;

    public function __construct(private readonly RouterInterface $router)
    {
    }

    private function getPager(QueryBuilder|Pagerfanta $qbOrPager, int $limit,): Pagerfanta
    {
        if ($qbOrPager instanceof QueryBuilder) {
            $qbOrPager = new Pagerfanta(new QueryAdapter($qbOrPager));
        }

        return $qbOrPager->setMaxPerPage($limit);
    }

    private function getSlicedResults(QueryBuilder|Pagerfanta $qbOrPager, Request $request): PaginatedCollection
    {
        $limit = $this->getLimit($request);
        $offset = $request->query->get('offset', 0);
        $page = (int) floor($offset / $limit) + 1;

        $pager = $this->getPager($qbOrPager, $limit, $page);

        $items = [];
        $dataSet = $pager->getAdapter()->getSlice($offset, $limit);

        foreach ($dataSet as $result) {
            $items[] = $result;
        }

        $paginatedCollection = new PaginatedCollection($items, $pager->getNbResults(), $limit);
        $paginatedCollection->setPage($page);

        return $paginatedCollection;
    }

    private function getPaginatedResults(
        QueryBuilder|Pagerfanta $qbOrPager,
        Request $request,
        string $route,
        array $routeParams = []
    ): PaginatedCollection
    {
        $page = $request->query->get('page', 1);
        $limit = $this->getLimit($request);

        $pager = $this->getPager($qbOrPager, $limit, $page);
        $pager->setCurrentPage($page);

        $items = [];

        $dataSet = $pager->getCurrentPageResults();

        foreach ($dataSet as $result) {
            $items[] = $result;
        }

        $paginatedCollection = new PaginatedCollection($items, $pager->getNbResults(), $limit);

        $routeParams = array_merge($routeParams, $request->query->all());

        $createLinkUrl = function ($targetPage) use ($route, $routeParams) {
            return $this->router->generate($route, array_merge(
                $routeParams,
                array('page' => $targetPage)
            ));
        };

        $paginatedCollection->addLink('self', $createLinkUrl($page));
        $paginatedCollection->addLink('first', $createLinkUrl(1));
        $paginatedCollection->addLink('last', $createLinkUrl($pager->getNbPages()));
        if ($pager->hasNextPage()) {
            $paginatedCollection->addLink('next', $createLinkUrl($pager->getNextPage()));
        }
        if ($pager->hasPreviousPage()) {
            $paginatedCollection->addLink('prev', $createLinkUrl($pager->getPreviousPage()));
        }

        $paginatedCollection->setPage($page);

        return $paginatedCollection;
    }

    public function createCollection(QueryBuilder|Pagerfanta $pager, Request $request, string $route, array $routeParams = []): PaginatedCollection
    {
        if ($request->query->has('offset')) {
            return $this->getSlicedResults($pager, $request);
        }

        return $this->getPaginatedResults($pager, $request, $route, $routeParams);
    }

    private function getLimit(Request $request): int
    {
        $limit = (int) $request->query->get('limit', self::LIMIT);
        if (self::UNEXPECTED_LIMIT === $limit) {
            $request->query->set('limit', self::LIMIT);

            return self::LIMIT;
        }

        return $limit;
    }
}
