<?php

namespace App\Service;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

class FormErrorsWrapper
{
    /**
     * @param FormInterface $form
     *
     * @return array
     */
    public function wrapFormErrors(FormInterface $form): array
    {
        $errors = [];

        /** @var FormError[] $formErrors */
        $formErrors = $form->getErrors();

        foreach ($formErrors as $error) {
            $err = [];
            $err['message'] = strip_tags(preg_replace('/>.*?</s', '', $error->getMessage()));
            $err['errorCode'] = Response::HTTP_BAD_REQUEST;
            $errors[] = $err;
        }

        if (!empty($errors)) {
            $errors = ['errors' => $errors];
        }

        foreach ($form->all() as $key => $child) {
            if ($child instanceof FormInterface && $err = $this->wrapFormErrors($child)) {
                $errors['children'][$key] = $err;
            }
        }

        return $errors;
    }
}
