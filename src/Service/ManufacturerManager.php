<?php

namespace App\Service;

use App\Model\ManufacturerSearch;
use App\Pagination\PaginatedCollection;
use App\Repository\ManufacturerRepository;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\RequestStack;

readonly class ManufacturerManager
{
    public function __construct(
        private ManufacturerRepository $manufacturerRepository,
        private RequestStack $requestStack,
        private PaginationFactory $paginationFactory
    )
    {
    }

    public function search(ManufacturerSearch $manufacturerSearch): PaginatedCollection
    {
        // get the query builder
        $qb = $this->manufacturerRepository->createQueryBuilder('m');
        $qb->leftJoin('m.vehicles', 'v');

        if ($manufacturerSearch->getName()) {
            $qb ->andWhere($qb->expr()->like('m.name', '%:manufacturerName%'))
                ->setParameter('manufacturerName', '%'.$manufacturerSearch->getName().'%');
        }

        if ($manufacturerSearch->getVehicleName()) {
            $qb ->andWhere($qb->expr()->like('v.name', ':vehicleName'))
                ->setParameter('vehicleName', '%'.$manufacturerSearch->getVehicleName().'%');
        }

        if ($manufacturerSearch->getVehicleType()) {
            $qb->andWhere($qb->expr()->eq('v.type', ':type'))
                ->setParameter(':type', $manufacturerSearch->getVehicleType());
        }

        // get the results
        $pager = new Pagerfanta(new QueryAdapter($qb));

        return $this->paginationFactory
            ->createCollection(
                $pager,
                $this->requestStack->getMainRequest(),
                'api_vehicle_list'
            );
    }
}
