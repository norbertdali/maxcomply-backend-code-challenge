<?php

namespace App\Service;

use App\Entity\Vehicle;
use App\Form\VehicleType;
use App\Pagination\PaginatedCollection;
use App\Repository\VehicleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

readonly class VehicleManager
{
    public function __construct(
        private VehicleRepository $vehicleRepository,
        private RequestStack $requestStack,
        private PaginationFactory $paginationFactory,
        private EntityManagerInterface $entityManager,
        private FormFactoryInterface $formFactory,
        private FormErrorsWrapper $errorsWrapper
    )
    {
    }

    public function getList(): PaginatedCollection
    {
        $vehicleQueryBuilder = $this->vehicleRepository->createQueryBuilder('v');
        $vehicleQueryBuilder
            ->innerJoin('v.manufacturer', 'm')
            ->innerJoin('v.engine', 'e')
        ;

        $pager = new Pagerfanta(new QueryAdapter($vehicleQueryBuilder));

        return $this->paginationFactory
            ->createCollection(
                $pager,
                $this->requestStack->getMainRequest(),
                'api_vehicle_list'
            );
    }

    public function deleteVehicle(Vehicle $vehicle): void
    {
        $this->entityManager->remove($vehicle);
        $this->entityManager->flush();
    }

    /**
     * @throws \JsonException
     */
    public function update(Vehicle $vehicle): Vehicle|array
    {
        $form = $this->formFactory->create(VehicleType::class, $vehicle);

        $request = $this->requestStack->getMainRequest();
        if (!$request) {
            return $vehicle;
        }

        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $form->submit($data, Request::METHOD_PATCH !== $request->getMethod());
        if (!$form->isValid()) {
            return $this->errorsWrapper->wrapFormErrors($form);
        }

        $this->entityManager->persist($vehicle);
        $this->entityManager->flush();
        $this->entityManager->refresh($vehicle);

        return $vehicle;
    }
}
