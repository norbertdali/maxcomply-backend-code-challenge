<?php

namespace App\Listener;

use App\Service\UserPasswordManager;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::prePersist, method: 'prePersist', entity: User::class)]
readonly class UserPasswordListener
{
    public function __construct(private UserPasswordManager $userPasswordManager)
    {
    }

    public function prePersist(User $user): void
    {
        if (null !== $user->getPlainPassword()) {
            $this->userPasswordManager->hashPassword($user);
        }
    }
}
