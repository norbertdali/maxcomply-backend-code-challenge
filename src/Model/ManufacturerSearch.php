<?php

namespace App\Model;

class ManufacturerSearch
{
    public ?string $name = null;
    public ?string $vehicleName = null;
    public ?string $vehicleType = null;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVehicleName(): ?string
    {
        return $this->vehicleName;
    }

    public function setVehicleName(?string $vehicleName): self
    {
        $this->vehicleName = $vehicleName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVehicleType(): ?string
    {
        return $this->vehicleType;
    }

    /**
     * @param string|null $vehicleType
     * @return $this
     */
    public function setVehicleType(?string $vehicleType): self
    {
        $this->vehicleType = $vehicleType;

        return $this;
    }
}
