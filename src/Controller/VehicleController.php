<?php

namespace App\Controller;

use App\Entity\Vehicle;
use App\Service\VehicleManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

#[Rest\Route('/api', name: 'api_')]
class VehicleController extends AbstractController
{
    /**
     * 2. Endpoint for retrieving all the technical details of a specific vehicle
     */
    #[Rest\Get('/vehicle/{id}', name: 'vehicle_detail')]
    public function getVehicleDetail(Vehicle $vehicle): Response
    {
        return $this->singleObjectView($vehicle, Response::HTTP_OK, 'detail');
    }

    #[Rest\Get('/vehicle', name: 'vehicle_list')]
    public function list(VehicleManager $vehicleManager): Response
    {
        return $this->collectionView(
            $vehicleManager->getList(),
            Response::HTTP_OK,
            'list'
        );
    }

    #[Rest\Delete('/vehicle/{id}', name: 'vehicle_delete')]
    public function delete(Vehicle $vehicle, VehicleManager $vehicleManager): Response
    {

        $vehicleManager->deleteVehicle($vehicle);

        return $this->singleObjectView(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * 3. Endpoint for updating a specific technical parameter of a vehicle
     *
     * @throws \JsonException
     */
    #[Rest\Patch('/vehicle/{id}', name: 'vehicle_patch')]
    // @Todo Methode PUT need more configuration to pass the validation currently is disabled
    //#[Rest\Put('/vehicle/{id}', name: 'vehicle_put')]
    public function patch(Vehicle $vehicle, VehicleManager $vehicleManager): Response
    {
        $responseObject = $vehicleManager->update($vehicle);

        return $this->singleObjectView(
            $responseObject,
            $responseObject instanceof Vehicle ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST,
            'detail'
        );
    }
}
