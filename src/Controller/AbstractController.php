<?php

namespace App\Controller;

use App\Pagination\PaginatedCollection;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

class AbstractController extends AbstractFOSRestController
{
    /**
     * @param mixed             $object
     * @param int               $statusCode
     * @param null|array|string $group
     * @param null|bool         $enableMaxDepth
     *
     * @return Response
     */
    protected function singleObjectView(
        mixed $object,
        int $statusCode,
        array|string $group = null,
        bool $enableMaxDepth = null
    ): Response
    {
        $clonedObject = is_object($object) ? clone $object : $object;

        $view = $this->view($clonedObject, $statusCode);

        return $this->createView($view, $group, $enableMaxDepth);
    }

    /**
     * @param PaginatedCollection $collection
     * @param int                 $statusCode
     * @param null|array|string   $group
     * @param array               $headers
     * @param null|bool           $enableMaxDepth
     *
     * @return Response
     */
    public function collectionView(
        PaginatedCollection $collection,
        int $statusCode,
        array|string $group = null,
        array $headers = [],
        bool $enableMaxDepth = null
    ): Response
    {
        // add to response header some extra collection data
        $headers = array_merge(
            $headers,
            [
                'X-Collection-Total' => $collection->getTotal(),
                'X-Collection-Count' => $collection->getCount(),
                'X-Collection-Pages' => $collection->getPages(),
            ]
        );

        $view = $this->view($collection->getItems(), $statusCode, $headers);

        return $this->createView($view, $group, $enableMaxDepth);
    }

    /**
     * @param View              $view
     * @param null|array|string $group
     * @param null|bool         $enableMaxDepth
     *
     * @return Response
     */
    private function createView(
        View $view,
        array|string $group = null,
        bool $enableMaxDepth = null,
    ): Response
    {
        // Use "default" group if empty.
        // By default, every attribute has the default group which
        // has the Serializer\Group annotation
        if (null === $group) {
            return $this->handleView($view);
        }

        $context = new Context();
        if (is_array($group)) {
            $context->setGroups($group);
        } else {
            $context->setGroups([$group]);
        }

        // by default maxDepth is 5
        if (true === $enableMaxDepth) {
            $context->enableMaxDepth();
        }

        $view->setContext($context);

        return $this->handleView($view);
    }
}
