<?php

namespace App\Controller;

use App\Entity\Manufacturer;
use App\Form\ManufacturerSearchType;
use App\Model\ManufacturerSearch;
use App\Service\ManufacturerManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

#[Rest\Route('/api', name: 'api_')]
class ManufacturerController extends AbstractController
{
    /**
     * 1. Endpoint for retrieving all the vehicle makers which are manufacturing a specific type of vehicle
     */
    #[Rest\Get('/manufacturer', name: 'manufacturer_list')]
    #[Rest\QueryParam(name: 'name', description: 'Manufacturer name', nullable: true)]
    #[Rest\QueryParam(name: 'vehicleName', description: 'Vehicle name', nullable: true)]
    #[Rest\QueryParam(name: 'vehicleType', description: 'Vehicle type (car|motorcycle|truck|bus)', nullable: true)]
    #[Rest\QueryParam(name: 'page', requirements: '\d+', default: 1, description: 'Current page', strict: true, nullable: true)]
    #[Rest\QueryParam(name: 'limit', requirements: '\d+', default: 10, description: 'Results per page', strict: true, nullable: true)]
    #[Rest\QueryParam(name: 'offset', requirements: '\d+', default: 0, description: 'Current offset, overwrite page value', strict: true, nullable: true)]
    public function list(Request $request, RouterInterface $router, ManufacturerManager $manufacturerManager): Response
    {
        $manufacturerSearch = new ManufacturerSearch();
        // create and submit form to populate ManufacturerSearch
        $this->createForm(ManufacturerSearchType::class, $manufacturerSearch, [
            'action' => $router->generate('api_manufacturer_list'),
        ])->submit($request->query->all());

        return $this->collectionView(
            $manufacturerManager->search($manufacturerSearch),
            Response::HTTP_OK,
            'list'
        );
    }

    #[Rest\Get('/manufacturer/{id}', name: 'manufacturer_detail')]
    public function detail(Manufacturer $manufacturer): Response
    {
        return $this->singleObjectView($manufacturer, Response::HTTP_OK, 'detail');
    }
}
