<?php

namespace App\Tests\Controller;

class ManufacturerControllerTest extends AbstractWebTestCase
{
    public function testUnauthorized(): void
    {
        $this->authorizedClient->request('GET', '/api/manufacturer', [], [], [
            // overwrite api-token
            'HTTP_api-token' => 'WRONG-TOKEN-kDgjFpIxn7ReP2BENBjyOBLEB2DURNoU87FnV5HYG3KoDj1nbTCm'
        ] );
        self::assertResponseStatusCodeSame(401);
    }

    public function testAuthorized(): void
    {
        $this->authorizedClient->request('GET', '/api/manufacturer');
        self::assertResponseIsSuccessful();
    }

    /**
     * @throws \JsonException
     */
    public function testWithParameters(): void
    {
        $this->authorizedClient->request('GET', '/api/manufacturer', [
            'vehicleName' => 'vehicle',
            'vehicleType' => 'car',
            'limit' => 20,
            'offset' => 1,
        ]);
        self::assertResponseIsSuccessful();
        $content = $this->authorizedClient->getResponse()->getContent();
        $response = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        self::assertNotEmpty($response);
    }
}
