<?php

namespace App\Tests\Controller;

use App\Controller\VehicleController;
use PHPUnit\Framework\TestCase;

class VehicleControllerTest extends AbstractWebTestCase
{
    public function testUnauthorized(): void
    {
        $this->authorizedClient->request('GET', '/api/vehicle', [], [], [
            // overwrite api-token
            'HTTP_api-token' => 'WRONG-TOKEN-kDgjFpIxn7ReP2BENBjyOBLEB2DURNoU87FnV5HYG3KoDj1nbTCm'
        ] );
        self::assertResponseStatusCodeSame(401);
    }

    public function testAuthorized(): void
    {
        $this->authorizedClient->request('GET', '/api/vehicle');
        self::assertResponseIsSuccessful();
    }

    /**
     * @throws \JsonException
     */
    public function testPatch(): void
    {
        $this->authorizedClient->jsonRequest('PATCH', '/api/vehicle/1', [
            'name' => 'New Vehicle Name'
        ]);
        $content = $this->authorizedClient->getResponse()->getContent();
        $response = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        self::assertEquals('New Vehicle Name', $response['name']);
        self::assertEquals('truck', $response['type']);
    }

    public function testInvalidParameter(): void
    {
        $this->authorizedClient->jsonRequest('PATCH', '/api/vehicle/1', [
            'random_name' => 'New Vehicle Name'
        ]);
        $content = $this->authorizedClient->getResponse()->getContent();
        $response = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        self::assertEquals('New Vehicle Name', $response['name']);
        self::assertEquals('truck', $response['type']);
    }
}
