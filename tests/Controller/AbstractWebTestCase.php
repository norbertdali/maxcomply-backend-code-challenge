<?php

namespace App\Tests\Controller;

use App\DataFixtures\AppFixtures;
use App\Kernel;
use Doctrine\ORM\Tools\SchemaTool;
use LogicException;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Dotenv\Dotenv;

abstract class AbstractWebTestCase extends WebTestCase
{
    protected KernelBrowser$authorizedClient;

    public function setUp(): void
    {
        $this->authorizedClient = static::createClient([], ['HTTP_api-token' => 'kDgjFpIxn7ReP2BENBjyOBLEB2DURNoU87FnV5HYG3KoDj1nbTCm']);
        $this->setUpDb();
    }

    protected function setUpDb(): void
    {
        (new Dotenv())->bootEnv(dirname(__DIR__).'/../.env.test');
        if ('test' !== self::$kernel->getEnvironment()) {
            throw new LogicException('Execution only in Test environment possible!');
        }

        $this->initDatabase();

        $fixture = self::$kernel->getContainer()->get(AppFixtures::class);

        $entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $fixture->load($entityManager);
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    private function initDatabase(): void
    {
        $entityManager = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $metaData = $entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($entityManager);
        $schemaTool->updateSchema($metaData);
    }
}
