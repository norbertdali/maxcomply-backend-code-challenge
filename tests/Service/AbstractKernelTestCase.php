<?php

namespace App\Tests\Service;

use App\Entity\ApiToken;
use App\Entity\User;
use App\Kernel;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Dotenv\Dotenv;

abstract class AbstractKernelTestCase extends KernelTestCase
{
    protected function setUpDb(): void
    {
        self::bootKernel();
        (new Dotenv())->bootEnv(dirname(__DIR__).'/../.env.test');
        if ('test' !== self::$kernel->getEnvironment()) {
            throw new LogicException('Execution only in Test environment possible!');
        }

        $this->initDatabase();

        $entityManager = self::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $user = new User();
        $user->setEmail('test@test.com');
        $user->setPlainPassword('123');
        $entityManager->persist($user);
        $entityManager->flush();

        $apiToken = new ApiToken();
        $apiToken->setToken('test-token-123');
        $apiToken->setUser($user);
        $entityManager->persist($apiToken);
        $entityManager->flush();
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    private function initDatabase(): void
    {
        $entityManager = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $metaData = $entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($entityManager);
        $schemaTool->updateSchema($metaData);
    }
}
