<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Service\UserPasswordManager;
use Exception;

class UserPasswordManagerTest extends AbstractKernelTestCase
{
    /**
     * @throws Exception
     */
    public function testHash(): void
    {
        /** @var UserPasswordManager $passwordManager */
        $passwordManager = static::getContainer()->get(UserPasswordManager::class);

        $user = new User();
        $user->setPlainPassword('!SuperSecret!');
        $this->assertEmpty($user->getPassword());
        $passwordManager->hashPassword($user);
        $this->assertNotEmpty($user->getPassword());
    }

    /**
     * @throws Exception
     */
    public function testHashFailed(): void
    {
        /** @var UserPasswordManager $passwordManager */
        $passwordManager = static::getContainer()->get(UserPasswordManager::class);

        $user = new User();
        $this->assertEmpty($user->getPlainPassword());
        $this->assertEmpty($user->getPassword());
        $passwordManager->hashPassword($user);
        $this->assertEmpty($user->getPassword());
    }
}
