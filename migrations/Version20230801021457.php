<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230801021457 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create all migrations';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE api_token (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, token VARCHAR(512) NOT NULL, INDEX IDX_7BA2F5EBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE engine (id INT AUTO_INCREMENT NOT NULL, engine_capacity SMALLINT NOT NULL, bore DOUBLE PRECISION NOT NULL, stroke DOUBLE PRECISION NOT NULL, fuel_type SMALLINT NOT NULL, number_of_cylinders INT DEFAULT NULL, position VARCHAR(100) NOT NULL, horse_power SMALLINT NOT NULL, number_of_speed SMALLINT NOT NULL, speed_type VARCHAR(3) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE manufacturer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE production_year (id INT AUTO_INCREMENT NOT NULL, vehicle_model_id INT NOT NULL, year DATE NOT NULL, INDEX IDX_60923823A467B873 (vehicle_model_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle (id INT AUTO_INCREMENT NOT NULL, engine_id INT NOT NULL, manufacturer_id INT NOT NULL, vehicle_model_id INT NOT NULL, name VARCHAR(255) NOT NULL, dimension INT NOT NULL, type VARCHAR(50) NOT NULL, INDEX IDX_1B80E486E78C9C0A (engine_id), INDEX IDX_1B80E486A23B42D (manufacturer_id), INDEX IDX_1B80E486A467B873 (vehicle_model_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicle_model (id INT AUTO_INCREMENT NOT NULL, manufacturer_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_B53AF235A23B42D (manufacturer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE api_token ADD CONSTRAINT FK_7BA2F5EBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE production_year ADD CONSTRAINT FK_60923823A467B873 FOREIGN KEY (vehicle_model_id) REFERENCES vehicle_model (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486E78C9C0A FOREIGN KEY (engine_id) REFERENCES engine (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486A23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486A467B873 FOREIGN KEY (vehicle_model_id) REFERENCES vehicle_model (id)');
        $this->addSql('ALTER TABLE vehicle_model ADD CONSTRAINT FK_B53AF235A23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE api_token DROP FOREIGN KEY FK_7BA2F5EBA76ED395');
        $this->addSql('ALTER TABLE production_year DROP FOREIGN KEY FK_60923823A467B873');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486E78C9C0A');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486A23B42D');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486A467B873');
        $this->addSql('ALTER TABLE vehicle_model DROP FOREIGN KEY FK_B53AF235A23B42D');
        $this->addSql('DROP TABLE api_token');
        $this->addSql('DROP TABLE engine');
        $this->addSql('DROP TABLE manufacturer');
        $this->addSql('DROP TABLE production_year');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE vehicle');
        $this->addSql('DROP TABLE vehicle_model');
    }
}
